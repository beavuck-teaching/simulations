# Simulations

## Compile and run

Makefile is currently just a rough draft, you should probably just use:
```
gcc -Wall TP[number]/main.c -o prog
./prog
```
... replacing `[number]` with the relevant number

## Reports and references

[Available here](https://drive.google.com/drive/folders/1QElYqtz2e5TNlBPriUG_u1S6qJ0HnkHO?usp=drive_link)
